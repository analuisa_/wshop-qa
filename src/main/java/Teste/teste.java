package Teste;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class teste {

	@Test
	public void fluxoFeliz() {
		
		WebDriverManager.chromiumdriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		WebElement userNameBox = driver.findElement(By.name("username"));
		WebElement passwordBox = driver.findElement(By.name("password"));
		WebElement commentsBox = driver.findElement(By.name("comments"));

		WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
		
		WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
		Select selectMultipleSelect = new Select(multipleSelect);
		
		WebElement dropdown = driver.findElement(By.name("dropdown"));
		Select selectDropdown = new Select(dropdown);

		WebElement radio1 = driver.findElement(By.xpath("//input[@value='rd1']"));
		
		WebElement buttonSubmit = driver.findElement(By.xpath("//input[@value='submit']"));
		
		userNameBox.sendKeys("QA Name");
		passwordBox.sendKeys("QA Senha");
		commentsBox.sendKeys("com");
		checkbox1.click();
		selectMultipleSelect.selectByValue("ms2");
		selectDropdown.selectByValue("dd3");
		radio1.click();
		
		buttonSubmit.click();
		
		Assert.assertEquals("QA Name", driver.findElement(By.id("_valueusername")).getText());
		Assert.assertEquals("QA Senha", driver.findElement(By.id("_valuepassword")).getText());
		
		
		
		
	}

}
